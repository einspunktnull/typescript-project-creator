#!/bin/bash

################### BEGIN #########################

echo "Hello $USER. Let's create an awesome typescript project."
thisDir=$(dirname "$(readlink -f "$0")")
source "$thisDir/functions.sh"


################### OBTAIN USER DEFINED DATA #########################

projectName=$(readProperty "Name" "awesome-typescript-project" ^[0-9a-zA-Z/@_-]+$)
projectDirectory=$(readProperty "Directory" "./$(basename "$projectName")" ^[0-9a-zA-Z/_.-]+$)
projectDescription=$(readProperty "Description" "This is an awesome typescript project description" )
authorName=$(readProperty "Author" "$USER")


################### CHECK FOR DESTINATON DIRECTORY #########################

supposedProjectDirectory=$(readlink -f "${PWD}/${projectDirectory}")
if [ -d "$supposedProjectDirectory" ]
then
  echo "Warning: Directory $supposedProjectDirector already exists!"
  echo "Existing files will be overriden. "
  proceed=$(readProperty "Do you wish to proceed?" false ^[yYnN]$)
  if [[ ! $proceed =~ ^[yY]$ ]]
  then
    echo "Project creation cancelled."
    exit 1
  fi
fi


################## COPY TEMPLATE #########################

thisScriptPath=$(readlink -f "$0")
thisDir=$(dirname "$thisScriptPath")
projectTemplateDir="$thisDir/template"
cp -r "$projectTemplateDir/." "$supposedProjectDirectory/"


################## MODIFY BASIC PROPERTIES #########################

packageJson="$supposedProjectDirectory/package.json"
replace "project-name" "$projectName" "$packageJson"
replace "project-description" "$projectDescription" "$packageJson"
replace "author-name" "$authorName" "$packageJson"


################### ADD BASIC DEPENDENCIES  #########################

dependencies="typescript"
devDependencies="nodemon rimraf concurrently @types/node source-map-support"
replaceDependencies "dependencies" "$dependencies" "$packageJson"
replaceDependencies "devDependencies" "$devDependencies" "$packageJson"


################### FIN  #########################

echo "Project successfully created at $supposedProjectDirectory."


################### INSTALL  #########################

install=$(readProperty "Do you wish to npm install?" false ^[yYnN]$)
if [[ ! $install =~ ^[yY]$ ]]
then
  exit 0
fi
npm --prefix "$supposedProjectDirectory" install "$supposedProjectDirectory"


################### START  #########################

startApp=$(readProperty "Do you wish to start app?" false ^[yYnN]$)
if [[ $startApp =~ ^[yY]$ ]]
then
  npm --prefix "$supposedProjectDirectory" run start
fi






function readProperty() {
  desc="$1"
  default="$2"
  preview=$default
  if [ "$default" = true ]
  then
    default="Y"
    preview='Y/n'
  else
    if [ "$default" = false ]
    then
      default="N"
      preview='y/N'
    fi
  fi
  regexp=${3:-^.*$}
#  echo "$regexp" >&2
#  echo "$default" >&2
  isValid=false
  while [ $isValid != true ]
  do
    read -p "$desc [$preview]: " value
    value=${value:-$default}
    if [[ ! $value =~ $regexp ]]
    then
      echo "Error: invalid input!" >&2
    else
      isValid=true
    fi
  done
  echo "$value"
}

function replace() {
  pattern="$1"
  replacer="$2"
  file="$3"
  sed --regexp-extended --in-place --expression="s/$pattern/$replacer/g" "$file"
}

function replaceDependencies() {
  field=($1)
  deps=($2)
  packageJson=($3)
  replacer="\"$field\": {"
  for index in ${!deps[*]}
  do
    package=${deps[$index]}
    version=$(npm show "$package" version)
    if [ "$index" != 0 ]
    then
      replacer+=","
    fi
    replacer+=$'\\n    '"\"$package\": \"^$version\""
  done
  replacer+=$'\\n  }'
  replacer="${replacer//\//\\/}"
#  echo "$replacer" >&2
  replace "\"$field\": \{\}" "$replacer" "$packageJson"

}